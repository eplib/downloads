SOURCE_DIRS = $(wildcard ../$(CORPUS)/texts/*)
SUBDIRS := $(patsubst ../$(CORPUS)/texts/%,%,$(SOURCE_DIRS))
SUBDIR_MAKEFILES := $(patsubst %,%/Makefile,$(SUBDIRS))

all clean: setup $(SUBDIRS) $(FORCE)

setup: $(SUBDIR_MAKEFILES)

%/Makefile: Makefile.template
	mkdir -p $(dir $@)
	cp Makefile.template $@

$(SUBDIRS): FORCE
	$(MAKE) -C $@ $(MAKECMDGOALS)

FORCE:

realclean:
	rm -rf $(SUBDIRS)

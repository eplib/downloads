<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.tei-c.org/ns/1.0"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="xs tei"
                version="2.0">

    <!-- Retrieve standardize parameter.-->

    <xsl:param name="standardize"/>

    <!-- Suppress excess spaces and indent. -->

    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <!-- Default identity templates. -->

    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- Eject  machine-generated_castlist and textual_notes divs. -->

    <xsl:template match="tei:div">
        <xsl:choose>
            <xsl:when test="@type='machine-generated_castlist' or @type= 'textual_notes'">
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*, node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Eject facsimile section. -->

    <xsl:template match="tei:facsimile">
    </xsl:template>

    <!-- Drop xml:id except for a few places where it might be useful. -->

    <xsl:template match="@xml:id">
        <xsl:choose>
            <xsl:when test="parent::tei:TEI or parent::tei:pb or parent::tei:ref or parent::tei:note">
                <xsl:copy>
                    <xsl:apply-templates select="."/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Our ref/@target attributes do not currently conform to the standard for same-document
         reference URIs with the '#' prefix, so add those here. But if the target is a
         marginal note, just drop the target attribute as an internal link to an immediately
         adjacent note on the same page does little good and confuses ePub and PDF generation.
    -->

    <xsl:key name="note-id" match="tei:note[not(@place) or @place!='margin']" use="@xml:id"/>
    <xsl:key name="marginal-note-id" match="tei:note[@place='margin']" use="@xml:id"/>

    <xsl:template match="@target">
        <xsl:choose>
            <xsl:when test="key('note-id', .)">
                <xsl:attribute name="target"><xsl:value-of select="concat('#', .)"/></xsl:attribute>
            </xsl:when>
            <xsl:when test="key('marginal-note-id', .)">
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="."/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
    <xsl:template match="tei:note[@place='margin']">
        <xsl:choose>
            <xsl:when test="descendant::tei:floatingText">
                <xsl:element name="note">
                    <xsl:copy-of select="@* except @place"/>
                    <xsl:apply-templates select="./*"/>
                </xsl:element>
            </xsl:when>
             <xsl:otherwise>
                <xsl:element name="note">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates select="./*"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 -->

    <!-- epub-preflight.xsl from the TEI Stylesheets drops all attributes from pb elements when there
         is a @facs attribute starting with "tcp" which gives us a bunch of [Page] markers in all e-book
         formats without any indication of what page. So parse our @xml:id which (usually) has both a page number
         and page side (a=left, b=right, c...d etc.=missing pages we've supplied) and use it to generate
         an @n attribute.  We need to handle page IDs in the format N01981-0002 as well as A79391-001-a.
         Our derived @n attribute will really give us more of a page image number than an actual page number.
         Keep the @xml:id attribute as-is.
    -->

    <xsl:template match="tei:pb">
        <xsl:choose>
            <!-- If the following element is another pb with nothing in between, just drop this one. -->
            <xsl:when test="following::*[1][self::tei:pb]">
            </xsl:when>
            <!-- Page breaks inside of a marginal note wreak all sorts of havoc with epub and pdf generation
                 so we'll just drop them. -->
            <xsl:when test="ancestor::tei:note[@place='margin']">
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="pb">
                    <xsl:variable name="pageID" select="substring-after(@xml:id, '-')"/>
                    <xsl:variable name="pageNum">
                        <xsl:choose>
                        <xsl:when test="contains($pageID, '-')">
                            <xsl:value-of select="string(number(substring-before($pageID, '-')))"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="string(number($pageID))"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="pageSide" select="substring-after($pageID, '-')"/>
                    <xsl:attribute name="n"><xsl:value-of select="concat($pageNum, $pageSide)"/></xsl:attribute>
                    <xsl:attribute name="xml:id"><xsl:value-of select="@xml:id"/></xsl:attribute>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- The TEI Stylesheets now expect the values of @rendition attributes to be declared as rendition elements
         in a tagsDecl section.  If it doesn't find the definitions internally, it will attempt to look them up
         externally, such that <hi rendition="something"> will trigger a lookup of a stylesheet named "something"
         and fail with file not found.  We currently have an unholy mix of @rend and @rendition with made-up
         values for the latter mostly designed to avoid having hi elements inside of words.  This will need some
         kind of clean-up and redesign, but for now we just rename @rendition to the more forgiving @rend and
         drop the preceding '#' (if any).
    -->

    <xsl:template match="@rendition">
        <xsl:attribute name="rend">
            <xsl:value-of select="replace(., '^#', '')"/>
        </xsl:attribute>
    </xsl:template>

    <!-- If <pc> needs a following space, add one, and drop empty pc elements. -->

    <xsl:template match="tei:pc">
        <xsl:choose>
            <xsl:when test="text()=''"> <!-- Drop empty pc elements -->
            </xsl:when>
            <xsl:when test="not(@join = 'right' or @join = 'both') and following::*[1] and not(parent::tei:q/tei:pc[last()] = node()) and not(parent::tei:quote/tei:pc[last()] = node())">
                <xsl:apply-templates select="text()"/>
                <xsl:text> </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="text()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- If <w> needs a following space, add one.
         Pick @reg over content when standard spellings have been chosen.
    -->

    <xsl:template match="tei:w">
        <xsl:choose>
            <xsl:when test="@reg and $standardize='true'">
                 <xsl:value-of select="@reg"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="text()"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="not(@join = 'right' or @join = 'both') and not(following::*[1][self::tei:w and (@join = 'left' or @join='both')]) and not(following::*[1][self::tei:pc and not(@join='right')]) and not(parent::tei:q/tei:w[last()] = node()) and not(parent::tei:quote/tei:w[last()] = node())">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
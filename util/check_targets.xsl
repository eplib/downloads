<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.tei-c.org/ns/1.0"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="xs tei"
                version="2.0">

    <xsl:output method="xml" omit-xml-declaration="yes" />
    <xsl:template match="attribute() | text() | comment() | processing-instruction()"/>

    <xsl:key name="xmlid" match="tei:note" use="@xml:id"/>

    <xsl:template match="tei:ref">
        <xsl:choose>

            <xsl:when test="key('xmlid', replace(@target, '#', ''))">
            </xsl:when>
            <xsl:when test="starts-with(@target, 'http:')">
            </xsl:when>
            <xsl:when test="starts-with(@target, 'https:')">
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>ref '</xsl:text>
                <xsl:value-of select="@xml:id"/>
                <xsl:text>' has missing target '</xsl:text>
                <xsl:value-of select="replace(@target, '#', '')"/>
                <xsl:text>'&#xa;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates/>
    </xsl:template>
</xsl:stylesheet>

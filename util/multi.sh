#/bin/sh

# This helper script will take advantage of multiple CPUs by
# running the individual Makefiles in all the subdirectories
# 3 at a time.  If make -j isn't working (e.g., under nohup)
# run this script from the top directory something like this:

#   nohup ./util/multi.sh > multi.log 2>&1 &

do_something () {
    dir=$(basename "$1")
    cd "$dir"
    make CORPUS=eptexts > "../$dir.log" 2>&1
    cd ..
}

export -f do_something

ls -1 -d */ | xargs -I {} -P 3 sh -c '
{
    echo "processing {}"
    do_something "{}"
}'

# Scaffolding to build downloads for the EarlyPrint Library

This project must sit alongside a checkout of a repository such as the eebotcp
repository from [here](https://bitbucket.org/eplib/eebotcp/src/master/) and
will refer to the directories under ../eebotcp/texts (or equivalent texts
sudirectory for a different corpus.  The name of the repository containing the
corpus of interest must be passed to the Makefile like so:

```sh
make CORPUS=eebotcp
```

  or

```
make CORPUS=evanstexts
```

To build downloads for a virtual combined corpus, see the [eptexts
repository](https://bitbucket.org/eplib/eptexts/src/master/) and build like so:

```sh
make CORPUS=eptexts
```

# Prerequisites

GNU make and some sort of Unix shell are assumed.

The TEI Consortium Stylesheets located on [GitHub](https://github.com/TEIC/Stylesheets)
must be installed or available somewhere in your PATH.

The [calibre](https://calibre-ebook.com) library and associated command-line tools
must be installed for e-book conversions. Versions prior to 5.4 have bugs that
prevent some e-book conversions from running, which very likely means you will need
to install a newer version following instructions on the calibre web site rather
than using whatever package may be available from your OS vendor.

# Process Outline

The basic process runs through the following steps, though not necessarily in exactly
this order:

1. Create a zipped copy of the upstream XML file for a slimmer download.

2. Use a local XSLT stylesheet to strip word-level tagging and clean up certain
encodings that bloat the file and/or cause trouble for downstream processing. The output
is an XML file without word-level tagging that is used as input by the following steps
but may also be of interest in its own right.

3. Produce a zipped copy of the XML file that is the output of step #2 in order to make
it available for download.

4. Use the TEI Consortium XSLT stylesheets to convert the XML file generated in step #2
to ePub format using a customized local CSS stylesheet.

5. Use the calibre command-line tools to convert the ePub generated in step #4 PDF format.

6. Repeat step #2 through step #5 to produce versions of each file with standardized
(rather than original) spellings.
